locals {
  lambda-zip-location = "outputs/function.zip"
}

data "archive_file" "init" {
  type        = "zip"
  source_file = "function.js"
  output_path = local.lambda-zip-location
}

resource "aws_lambda_function" "test_lambda" {
  filename      = local.lambda-zip-location
  function_name = "function"
  role          = aws_iam_role.lambda_role.arn
  handler       = "function.lambdaHandler"

  # source_code_hash = data.archive_file.lambda.output_base64sha256

  runtime = "nodejs14.x"

}
