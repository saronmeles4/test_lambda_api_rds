locals {
	resource_name     = "${var.app_name}.${terraform.workspace}"
	resource_name_sid = join("", [
		for segment in split(".", local.resource_name) :
		"${upper(substr(segment, 0, 1))}${substr(segment, 1, length(segment))}"
	])
}

terraform {
	required_providers {
		aws = {
			source  = "hashicorp/aws"
			version = "~> 5.30.0"
		}
		archive = {
			source  = "hashicorp/archive"
			version = "~> 2.2.0"
		}
	}
	required_version = ">= 1.6.2"
}

provider aws {
	profile = var.profile
	region  = var.region


	default_tags {
		tags = {
			method                = "terraform"
			"terraform.workspace" = terraform.workspace
			by                    = data.external.get_username.result.by
			app_name              = var.app_name
		}
	}
}

data aws_region this {}


data external get_username {
	program = [
		"pwsh", "-Command",
		"(aws sts --profile ${var.profile} get-caller-identity --query 'Arn' --output text) -split '/' | select -Last 1 | %%{ @{ by = $_ } } | ConvertTo-Json"
	]
}

data "aws_availability_zones" "available" {
  state = "available" # This filters the results to only include AZs that are available to your account
}
