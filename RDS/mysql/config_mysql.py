import json
import os
import secrets
import string
import sys

import boto3
import mysql.connector
from mysql.connector import Error
from more_itertools import one
import psutil


def create_users(profile, rds_arn, host, usernames: list[str]):
	try:
		root_password = get_root_password(profile, rds_arn)

		passwords = [make_password() for _ in usernames]

		with mysql.connector.connect(
				host=host,
				user="root",
				password=root_password,
				# database="datapoints"
		) as connection:
			with connection.cursor() as cursor:
				for username, password in zip(usernames, passwords):
					password = password
					cursor.execute(f"CREATE USER '{username}'@'%' IDENTIFIED BY '{password}';")
					cursor.execute(f"""GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, RELOAD, PROCESS, 
REFERENCES, INDEX, ALTER, SHOW DATABASES, CREATE TEMPORARY TABLES, LOCK TABLES, 
EXECUTE, REPLICATION SLAVE, REPLICATION CLIENT, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, 
ALTER ROUTINE, CREATE USER, EVENT, TRIGGER, GRANT OPTION
ON *.* TO '{username}'@'%';""")
					cursor.execute("FLUSH PRIVILEGES;")
			connection.commit()  # Commit the transactions after all users are created

		return passwords

	except Error as e:
		print(e)


def make_password():
	safe_special_chars = "!#$^&*()-_=+[]{};:,.<>?/|~"
	characters = string.ascii_letters + string.digits + safe_special_chars
	password = ''.join(secrets.choice(characters) for i in range(12))
	return password


def save_password_to_file(param, param1, param2):
	pass


def get_root_password(profile: str, rds_arn: str):
	session = boto3.session.Session(profile_name=profile)
	client = session.client(service_name='secretsmanager')
	secrets_list = client.list_secrets()['SecretList']

	s0 = {i['Key']: i['Value'] for i in secrets_list[0]['Tags']}
	s1 = {i['Key']: i['Value'] for i in secrets_list[1]['Tags']}

	secret_name = one(
		(
			secret['Name'] for secret in secrets_list if
			has_rds_owning_service_tag(secret['Tags'])
			and arn_matches(secret['Tags'], rds_arn)
		),
		None)

	get_secret_value_response = client.get_secret_value(SecretId=secret_name)
	the_password = json.loads(get_secret_value_response['SecretString'])['password']
	return the_password


def has_rds_owning_service_tag(tags: list):
	return any(tag['Key'] == 'aws:secretsmanager:owningService'
	           and tag['Value'] == 'rds' for tag in tags)


def arn_matches(tags, arn):
	return any(tag['Key'] == 'aws:rds:primaryDBInstanceArn'
	           and tag['Value'] == arn for tag in tags)


def main():
	aws_profile = os.getenv('aws_profile')
	rds_arn = os.getenv('rdb_arn')
	rdb_domain = os.getenv('rdb_domain')
	usernames = os.getenv('usernames')

	if not_terraform():  # just for local debug!!!
		aws_profile = 'datapoints'
		rds_arn = 'arn:aws:rds:eu-north-1:123456789012:db:datapoints-default-rdb'
		rdb_domain = 'datapoints-default-rdb.cdg7evuvvynt.eu-north-1.rds.amazonaws.com'

	if aws_profile is None or rds_arn is None or rdb_domain is None or usernames is None:
		print("Required variable(s) 'aws_profile', 'rdb_arn', 'rdb_domain' or 'usernames' not set. Exiting...")
		return

	usernames = usernames.split(',')

	# host = 'datapoints-default-rdb.cdg7evuvvynt.eu-north-1.rds.amazonaws.com'
	passwords = create_users(aws_profile, rds_arn, rdb_domain, usernames)

	print(passwords)


# store_secret_in_vault('datapoints', 'eli', 'shush!')
# get_root_password(profile, rds_arn)

def find_first_non_python_parent_process() -> psutil.Process:
	process = psutil.Process(os.getpid())
	while process.name() == 'python.exe':
		process = psutil.Process(process.ppid())
	return process


def is_pycharm(): return find_first_non_python_parent_process().name() == 'pycharm64.exe'


def is_terraform(): return find_first_non_python_parent_process().name() == 'terraform.exe'


def not_terraform(): return not find_first_non_python_parent_process().name() == 'terraform.exe'


if __name__ == '__main__':
	main()
