variable region {
	type        = string
	description = "default geo region to use"
	default     = "eu-north-1" # not suitable for route53, cloudfront, ...
}

variable profile {
	type        = string
	description = "profile to use from ~/.aws/credentials"
}

variable app_name {
	type        = string
	description = "app-specific resource name prefix"
}

variable rds_usernames {
	type        = string
	description = "comma-separated usernames to create for the RDS instance, e.g. 'user1,user2'"
}

variable vault_name {
	type        = string
	description = "valut name to use"
}
variable "AWS_REGION" {
  type    = string
  default = "us-east-1"
}
variable "AWS_ACCESS_KEY" {

}
variable "AWS_SECRET_KEY" {

}