
# resource "aws_db_instance" "example" {
#   identifier         ="mysql-db-demo"
#   allocated_storage    = 20
#   storage_type         = "gp2"
#   engine               = "mysql"
#   engine_version       = "5.7"
#   instance_class       = "db.t2.micro"
#   username             = "admin"
#   password             = "test123db"
#   parameter_group_name = "default.mysql5.7"
#   skip_final_snapshot  = true
# }



# resource "null_resource" "db_setup" {
#   provisioner "local-exec" {
#     command = "mysql -h ${aws_db_instance.example.mysql-db-demo.cjqoqaiukfx1.us-east-1.rds.amazonaws.com} -u ${aws_db_instance.example.admin} -p${aws_db_instance.example.test123db} >> setup.sql"
#   }
#   depends_on = [aws_db_instance.example]
# }

# resource "null_resource" "insert_cities" {
#   provisioner "local-exec" {
#     command = "mysql -h ${aws_db_instance.example.mysql-db-demo.cjqoqaiukfx1.us-east-1.rds.amazonaws.com} -u ${aws_db_instance.example.admin} -p${aws_db_instance.example.test123db} >> insert_cities.sql"
#   }
#   depends_on = [aws_db_instance.example, null_resource.db_setup]
# }


# Test Code

# data aws_secretsmanager_secret our_vault {
# 	name = var.vault_name
# }

# data aws_secretsmanager_secret_version datapoints_secret {
# 	secret_id = data.aws_secretsmanager_secret.our_vault.id
# }

resource aws_db_instance datapoints_rdb {
	identifier                  = "datapoints-${terraform.workspace}-rdb"
	allocated_storage           = 20 # in Gigabytes (free tier only allows 20GB, right?)
	storage_type                = "gp2"
	engine                      = "mysql"
	engine_version              = "8.0.34"
	instance_class              = "db.t3.micro"
	skip_final_snapshot         = true
	ca_cert_identifier          = "rds-ca-ecc384-g1" # see rdb_ca_cert_identifier.adoc
	publicly_accessible         = true
	username                    = "root"
	manage_master_user_password = true

	db_subnet_group_name   = aws_db_subnet_group.rdb_subnet_group.name
	vpc_security_group_ids = [aws_security_group.rdb_security_group.id]

	provisioner "local-exec" {
		interpreter = ["./venv312/Scripts/python.exe", "-O"]
		command = "./mysql/config_mysql.py"

		environment = {
			aws_profile = var.profile
			rdb_arn     = aws_db_instance.datapoints_rdb.arn
			rdb_domain  = aws_db_instance.datapoints_rdb.address
			usernames   = var.rds_usernames
		}
	}
}

resource null_resource test { # todo: remove it before merging to main!
	provisioner "local-exec" {
		interpreter = ["./venv312/Scripts/python.exe", "-O"]
		command = "./mysql/config_mysql.py"

		environment = {
			aws_profile = var.profile
			rdb_arn     = aws_db_instance.datapoints_rdb.arn
			rdb_domain  = aws_db_instance.datapoints_rdb.address
			usernames   = var.rds_usernames
		}
	}
}

resource aws_vpc rdb_vpc {
	cidr_block           = "10.0.0.0/16" # 65,536 IP addresses
	enable_dns_support   = true
	enable_dns_hostnames = true
	tags                 = { Name = "datapoints-${terraform.workspace}-rdb-vpc" }

}

resource aws_internet_gateway rdb_igw {
	vpc_id = aws_vpc.rdb_vpc.id
	tags   = { Name = "datapoints-${terraform.workspace}-rdb-igw" }
}

resource aws_route_table rdb_route_table {
	vpc_id = aws_vpc.rdb_vpc.id
	route {
		cidr_block = "0.0.0.0/0"
		gateway_id = aws_internet_gateway.rdb_igw.id
	}
	tags = { Name = "datapoints-${terraform.workspace}-rdb-route-table" }
}

#resource aws_route_table_association rdb_rta {
#  count = length(aws_subnet.rdb_3_subnets)
#
#  subnet_id      = aws_subnet.rdb_3_subnets[count.index].id
#  route_table_id = aws_route_table.rdb_route_table.id
#}

resource aws_main_route_table_association rdb_main_rta {
	vpc_id         = aws_vpc.rdb_vpc.id
	route_table_id = aws_route_table.rdb_route_table.id
}

resource aws_subnet rdb_3_subnets {
	count             = 3
	vpc_id            = aws_vpc.rdb_vpc.id
	cidr_block        = cidrsubnet(aws_vpc.rdb_vpc.cidr_block, 8, count.index) # 256 IP addresses
	availability_zone = data.aws_availability_zones.available.names[count.index]
	tags              = { Name = "datapoints-${terraform.workspace}-rdb-subnet-${count.index}" }
}

resource aws_db_subnet_group rdb_subnet_group {
	name       = "name-datapoints-${terraform.workspace}-rdb-subnet-group"
	subnet_ids = aws_subnet.rdb_3_subnets.*.id
	tags       = { Name = "datapoints-${terraform.workspace}-rdb-subnet-group" }
}

resource aws_security_group rdb_security_group {
	name        = "name-datapoints-${terraform.workspace}-rdb-security-group"
	description = "Allow access to the RDB from the VPC"
	vpc_id      = aws_vpc.rdb_vpc.id

	ingress {
		description = "MySQL"
		from_port   = 3306
		to_port     = 3306
		protocol    = "tcp"
		cidr_blocks = ["0.0.0.0/0"] # allow access from anywhere
		#cidr_blocks = [aws_vpc.rdb_vpc.cidr_block] # restrict to VPC
	}

	egress {
		from_port   = 0
		to_port     = 0
		protocol    = "-1"
		cidr_blocks = ["0.0.0.0/0"]
	}
	tags = { Name = "datapoints-${terraform.workspace}-rdb-security-group" }
}

#output "db_connection_string" { # todo: remove it before merging to main!
#  value = "mysql://${aws_db_instance.datapoints_rdb.username}:password_here@${aws_db_instance.datapoints_rdb.endpoint}/db_name_here"
#}